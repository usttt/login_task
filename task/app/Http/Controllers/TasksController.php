<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tasks;

class TasksController extends Controller
{
    protected $task_model;

    public function __construct(Tasks $task_model)
    {
        $this->task_model = $task_model;
    }

    public function index()
    {
        return view('tasks');
    }

    public function getTasks()
    {
        $tasks  =  $this->task_model->getAll();

        if($tasks->count() < 1)
        {
            $response = [
                'success' => false,
                'data'    => [],
                'message' => 'Данные не найдены'
            ];

            return response()->json($response, 200);

        }

        $response = [
            'success' => true,
            'data'    => $tasks,
            'message' => 'Данные найдены'
        ];

        return response()->json($response, 200);
    }


    public function getTaskId($id)
    {
        $task  =  $this->task_model->getId($id);

        if(is_null($task))
        {
            $response = [
                'success' => false,
                'data'    => [],
                'message' => 'Данные не найдены'
            ];

            return response()->json($response, 200);

        }

        $response = [
            'success' => true,
            'data'    => $task,
            'message' => 'Данные найдены'
        ];

        return response()->json($response, 200);
    }

    public function add()
    {
        return view('tasks_add');
    }

    public function create(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'name' => ['required', 'max:100'],
            'task' => ['required', 'max:500'],
        ]);

        if($validate->fails())
        {
            $response = [
                'success' => false,
                'data'    => [],
                'message' => ['errors' => $validate->errors()],
            ];
    
            return response()->json($response, 200);
        }
        
        $task = $this->task_model->store($request->all());

        if($task)
        {
            $response = [
                'success' => true,
                'data'    => [],
                'message' => ['success' => 'Задача успешно создана'],
            ];
    
            return response()->json($response, 200);
        }

        $response = [
            'success' => false,
            'data'    => [],
            'message' => ['errors' => ['Не удалось создать задачу']],
        ];

        return response()->json($response, 200);
    }

    public function edit($id)
    {
        $task = Tasks::find($id);
        
        return view('tasks_update', ['task' => $task]);
    }

    public function update(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'name'  =>  ['required', 'max:100'],
            'task'  =>  ['required', 'max:500'],
            'id'    =>  ['required', 'numeric'],
        ]);

        if($validate->fails())
        {
            $response = [
                'success' => false,
                'data'    => [],
                'message' => ['errors' => $validate->errors()],
            ];
    
            return response()->json($response, 200);
        }
        
        $task = $this->task_model->taskUpdate($request->all());

        if($task)
        {
            $response = [
                'success' => true,
                'data'    => [],
                'message' => ['success' => 'Задача успешно изменена'],
            ];
    
            return response()->json($response, 200);
        }
        
        $response = [
            'success' => false,
            'data'    => [],
            'message' => ['errors' => ['Не удалось обновить задачу']],
        ];

        return response()->json($response, 200);
    }

    public function remove($id)
    {
        $task = $this->task_model->remove($id);

        if($task)
        {
            return back()->with('message', 'Успено удалена задача');
        }
        
        return back()->with('error', 'Данный запись найден');
    }
}
