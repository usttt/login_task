<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
    protected $table = 'tasks';

    protected $fillable = ['name', 'to_do', 'status'];

    public function getAll()
    {
        $tasks = Tasks::all();

        return $tasks;
    }

    public function getId($id)
    {
        $task = Tasks::find($id);

        return $task;
    }

    public function store($request)
    {
        $task = new Tasks();

        $task->name  = $request['name'];
        $task->to_do = $request['task'];
        $task->save();

        return true;
    }

    public function taskUpdate($request)
    {
        $task = Tasks::find($request['id']);

        if(is_null($task))
        {
            return false;
        }

        $task->name         = $request['name'];
        $task->to_do        = $request['task'];
        $task->status       = $request['status'];
        $task->updated_at   = now();
        $task->save();

        return true;
    }

    public function remove($id)
    {
        $task = Tasks::find($id);

        if(is_null($task))
        {
            return false;
        }
        
        $task->delete($id);

        return true;
  
    }
}
