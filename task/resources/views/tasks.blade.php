@extends('partials.layouts')
@section('content')
<div class="container" style="margin-top:50px;">
    <a href="{{url('tasks-add')}}" class="btn btn-info">Добавить</a>
    
    @if(session()->has('message'))
        <div class="alert alert-success mt-4 col-4" role="alert">
            {{session()->get('message')}}
        </div>
    @endif

    @if(session()->has('error'))
        <div class="alert alert-danger mt-4 col-4" role="alert">
            {{session()->get('error')}}
        </div>
    @endif

    <table class="table table-striped mt-5" id="tasks_all">
        
    </table>
</div>


<script>
    
    $.ajax({
        type:'GET',
        url:'/get-tasks',
        dataType: 'json',
        /*function (response) {
            console.log(response);
            if (response.data.success) {
                console.log(1);
            }
            else{
                console.log(2);
            }
            
        }*/
        success: function(data) {
        // какие-то действия с полученными данными data
            
            if(data.data.length > 0){
                $('#tasks_all').append('<thead>'
                                            +'<tr>'
                                                +'<th scope="col">#</th>'
                                                +'<th scope="col">Название</th>'
                                                +'<th scope="col">Задача</th>'
                                                +'<th scope="col">Статус</th>'
                                                +'<th scope="col">Создан</th>'
                                                +'<th scope="col">Изменен</th>'
                                                +'<th scope="col">Действие</th>'
                                            +'</tr>'
                                        +'</thead>'
                                        +'<tbody>'
                                        +'</tbody>'
                                        );

                $.each(data.data, function(index, item) {
                    console.log(item);
                    if(item.status == 0)
                    {
                        item.status = 'Процессе';
                    }
                    else{
                        item.status = 'Выполнено';
                    }
                       $('tbody').append('<tr>'
                                            +'<th scope="row">'+item.id+'</th>'
                                            +'<td>'+item.name+'</td>'
                                            +'<td>'+item.to_do+'</td>'
                                            +'<td>'+item.status+'</td>'
                                            +'<td>'+item.created_at+'</td>'
                                            +'<td>'+item.updated_at+'</td>'
                                            +'<td>'
                                            +'<a class="btn btn-sm btn-warning" href="{!!url("task-edit")!!}/'+item.id+'">Обновить</a>'
                                            +'<a class="btn btn-sm btn-danger ml-2" href="{!!url("task-delete")!!}/'+item.id+'">Удалить</a>'
                                            +'</td>'
                                        +'</tr>'
    
                                        );
                });
            }
        
        }
    });
    
</script>
@endsection